//
//  PedirNumeroViewController.swift
//  Waitless
//
//  Created by Daniel Romero on 10-07-16.
//  Copyright © 2016 Debak. All rights reserved.
//

import UIKit
import Alamofire
class PedirNumeroRenovaciónViewController: UIViewController {

    @IBOutlet weak var numeroActual: UILabel!
    @IBOutlet weak var siguienteNumero: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tabBarController?.tabBar.hidden = true
        actualizar()
        
        self.navigationItem.backBarButtonItem?.title = "Volver"
        
        setupNavigationBar()
     
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
  
    func actualizar(){
        Alamofire.request(.GET, "http://api.waitless.zeek.cl/queues/1", parameters: nil)
            .responseJSON { response in
                //print(response.request)  // original URL request
                //print(response.response) // URL response
                //print(response.data)     // server data
                //print(response.result)   // result of response serialization
                
                if let JSON = response.result.value {
                    print("JSON: \(JSON)")
                    
                    let response                  = JSON as! NSDictionary
                    
                    let data:NSDictionary         = response.objectForKey("data") as! NSDictionary
                    
                    let current_number:NSString         = (data.objectForKey("current_number") as! NSString)
                    
                    let last_number:NSString         = (data.objectForKey("last_number") as! NSString)
                    
                    let next_number:NSInteger  = Int(last_number as String)! + 1
                    
                    self.numeroActual.text = current_number as String
                    self.siguienteNumero.text = String(next_number)
                }
        }
        
        
        
    }

    
    func setupNavigationBar(){
        self.navigationItem.leftBarButtonItem = UIBarButtonItem()
        
        self.navigationItem.leftBarButtonItem!.tintColor = UIColor.whiteColor()
        self.navigationItem.leftBarButtonItem =  UIBarButtonItem(title: "Volver", style:   UIBarButtonItemStyle.Plain, target: self, action: #selector(PedirNumeroRenovaciónViewController.btn_clickedLeft(_:)))
    }

    func btn_clickedLeft(sender: UIBarButtonItem) {
        self.dismissViewControllerAnimated(true, completion: {});
    }
    
    @IBAction func TomarNumero(sender: AnyObject) {
        
        PedirNumero()
        
        let preferences = NSUserDefaults.standardUserDefaults()
        
        let currentLevelKey = "StatusNumeroRenovacion"
        
        let currentLevel = 1
            preferences.setInteger(currentLevel, forKey: currentLevelKey)
        
        //  Save to disk
        let didSave = preferences.synchronize()
        
        if !didSave {
            //  Couldn't save (I've never seen this happen in real world testing)
        }
        
    }
    
    func PedirNumero(){
        Alamofire.request(.POST, "http://api.waitless.zeek.cl/queues/1/attentions", parameters: nil)
            .responseJSON { response in
                //print(response.request)  // original URL request
                //print(response.response) // URL response
                //print(response.data)     // server data
                //print(response.result)   // result of response serialization
                
                if let JSON = response.result.value {
                    print("JSON: \(JSON)")
                    
                    let response                  = JSON as! NSDictionary
                    
                    let data:NSDictionary         = response.objectForKey("data") as! NSDictionary
                    
                    let queue:NSDictionary        = data.objectForKey("queue") as! NSDictionary
                    
                    let last_number:NSInteger         = (queue.objectForKey("last_number") as! NSInteger)
                    
                    let preferences = NSUserDefaults.standardUserDefaults()
                    
                    let currentLevelKey = "NumeroPedidoRenovacion"
                    
                    let currentLevel = last_number
                    preferences.setInteger(currentLevel, forKey: currentLevelKey)
                    
                    //  Save to disk
                    let didSave = preferences.synchronize()
                    
                        //let vc : AnyObject! = self.storyboard!.instantiateViewControllerWithIdentifier("NumeroPedido")
                        //self.showViewController(vc as! NumeroPedidoViewController, sender: vc)
                 
                        self.performSegueWithIdentifier("lastView", sender: self)
                    
                    if !didSave {
                        //  Couldn't save (I've never seen this happen in real world testing)
                    }

                    
                }
        }
        
        
    }

}
