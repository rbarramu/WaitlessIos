//
//  NumeroPedidoViewController.swift
//  Waitless
//
//  Created by Daniel Romero on 11-07-16.
//  Copyright © 2016 Debak. All rights reserved.
//

import UIKit

class NumeroPedidoViewController: UIViewController {

    @IBOutlet weak var numeroAtencion: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
        let preferences = NSUserDefaults.standardUserDefaults()
        
        let currentLevelKey = "NumeroPedidoRenovacion"
        
        if preferences.objectForKey(currentLevelKey) == nil {
            //  Doesn't exist
            
        } else {
            let currentLevel = preferences.integerForKey(currentLevelKey)
            
            print(currentLevel)
            
            numeroAtencion.text = String(currentLevel)
        }

       
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
  
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
