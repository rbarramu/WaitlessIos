//
//  LoginViewController.swift
//  Waitless
//
//  Created by Rocio Barramuño on 25-08-16.
//  Copyright © 2016 Debak. All rights reserved.
//

import UIKit
import Alamofire
import PKHUD
import PopupDialog

class LoginViewController: UIViewController {
    @IBOutlet weak var ingresaRut: UITextField!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func RevisarRut(){
        
        PKHUD.sharedHUD.contentView = PKHUDProgressView()
        PKHUD.sharedHUD.show()
        
        var newRut:NSString = ""
        if ingresaRut.text!.characters.contains("k") {
            
            
            newRut = ingresaRut.text!.replacingOccurrences(of: "k", with: "0", options: NSString.CompareOptions.literal, range: nil) as NSString
            
            
        }else if ingresaRut.text!.characters.contains("K"){
            print("Encontre K")
            newRut = ingresaRut.text!.replacingOccurrences(of: "K", with: "0", options: NSString.CompareOptions.literal, range: nil) as NSString
        }else{
            newRut = ingresaRut.text! as NSString
        }
        
        Pushbots.sharedInstance().tag([newRut])
        
        let preferences = UserDefaults.standard
        
        preferences.set(newRut, forKey: "RutUsuario")
        
        //  Save to disk
        let didSave = preferences.synchronize()
        
        if !didSave {
            //  Couldn't save (I've never seen this happen in real world testing)
        }
        
        PKHUD.sharedHUD.hide(afterDelay: 1.0) { progress in
            self.performSegue(withIdentifier: "TabBar", sender: self)

        }

    }

    @IBAction func Entrar(_ sender: AnyObject) {
        
        if ingresaRut.text!.range(of: "-") == nil && ingresaRut.text!.range(of: ".") == nil{
            
            if CVZPChileanRUT.isValidRUT(ingresaRut.text) {
                
                if connectedToNetwork() == true{
                    print("Connected")
                    RevisarRut()
                }else{
                    print("Not connected")
                    let title = "WAITLESS"
                    let message = "Lo sentimos, no tienes internet"
                    let image = UIImage(named: "AppIcon")
                    
                    // Create the dialog
                    let popup = PopupDialog(title: title, message: message, image: image)
                    
                    
                    // Create second button
                    let buttonTwo = DefaultButton(title: "Entendido") {
                        
                    }
                    
                    
                    
                    // Add buttons to dialog
                    popup.addButtons([buttonTwo,])
                    
                    // Present dialog
                    self.present(popup, animated: true, completion: nil)
                }
                
                
               
                
                
            } else {
                self.performSegue(withIdentifier: "error", sender: self)
            }

        }else{
                self.performSegue(withIdentifier: "error", sender: self)
        }
        
        
        
    }

    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillShow, object: self.view.window)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillHide, object: self.view.window)
    }
    
    func keyboardWillHide(_ sender: Notification) {
        let userInfo: [AnyHashable: Any] = (sender as NSNotification).userInfo!
        let keyboardSize: CGSize = (userInfo[UIKeyboardFrameBeginUserInfoKey]! as AnyObject).cgRectValue.size
        self.view.frame.origin.y += keyboardSize.height
    }
    
    func keyboardWillShow(_ sender: Notification) {
        let userInfo: [AnyHashable: Any] = (sender as NSNotification).userInfo!
        let keyboardSize: CGSize = (userInfo[UIKeyboardFrameBeginUserInfoKey]! as AnyObject).cgRectValue.size
        let offset: CGSize = (userInfo[UIKeyboardFrameEndUserInfoKey]! as AnyObject).cgRectValue.size
        
        if keyboardSize.height == offset.height {
            UIView.animate(withDuration: 0.1, animations: { () -> Void in
                self.view.frame.origin.y -= keyboardSize.height
            })
        } else {
            UIView.animate(withDuration: 0.1, animations: { () -> Void in
                self.view.frame.origin.y += keyboardSize.height - offset.height
            })
        }
    }


}
