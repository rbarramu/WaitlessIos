//
//  CustomCalloutView.swift
//  CustomCalloutView
//
//  Created by Malek T. on 3/10/16.
//  Copyright © 2016 Medigarage Studios LTD. All rights reserved.
//

import UIKit

class CustomCalloutView: UIView {

    @IBOutlet var starbucksImage: UIImageView!
    @IBOutlet var starbucksName: UILabel!
    @IBOutlet weak var buttonStore: UIButton!
    
    
    @IBAction func buttonStore(_ sender: AnyObject) {
        
         NotificationCenter.default.post(name: Notification.Name(rawValue: "selectPoint"), object: nil)
        
    }
    
}
