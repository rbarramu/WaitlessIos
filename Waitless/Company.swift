//
//  Company.swift
//  Waitless
//
//  Created by Daniel Romero on 10/31/16.
//  Copyright © 2016 Debak. All rights reserved.
//

import RealmSwift

class Company: Object {
    dynamic var name: String = ""
    dynamic var id: Int = 0
    dynamic var service: String = ""
    dynamic var tag: String = ""
    dynamic var imgURL: String = ""
}
