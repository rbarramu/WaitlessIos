//
//  VerEstadoRenovaciónViewControllerSeconds.swift
//  Waitless
//
//  Created by Rocio Barramuño on 18-08-16.
//  Copyright © 2016 Debak. All rights reserved.
//

import UIKit
import Alamofire
import PKHUD
import PopupDialog
import BTNavigationDropdownMenu

class VerEstadoRenovaciónViewControllerSeconds: UIViewController {
    

    @IBOutlet weak var Pretitulo: UILabel!
    @IBOutlet weak var Numero: UILabel!
    @IBOutlet weak var Seccion: UILabel!
    @IBOutlet weak var MiNumero: UILabel!
    @IBOutlet weak var Actualiza: UIButton!
    @IBOutlet weak var TiempoEspera: UILabel!
    @IBOutlet weak var titulo: UILabel!
    
    var menuView: BTNavigationDropdownMenu!
    var queueID: Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        if connectedToNetwork() == true{
            print("Connected")
            menuTab()
            actualizarCola()
            actualizarNumero()
            actualizarTiempoEspera()
           
        }else{
            print("Not connected")
            // Prepare the popup assets
            let title = "WAITLESS"
            let message = "Lo sentimos, no tienes internet"
            let image = UIImage(named: "AppIcon")
            
            // Create the dialog
            let popup = PopupDialog(title: title, message: message, image: image)
            
            
            // Create second button
            let buttonTwo = DefaultButton(title: "Entendido") {
                
            }
            
           // Add buttons to dialog
            popup.addButtons([buttonTwo,])
            
            // Present dialog
            self.present(popup, animated: true, completion: nil)
            

        }
        
    }
    
    
    func menuTab(){
        let items = [String]()
        self.navigationController?.navigationBar.isTranslucent = false
        UINavigationBar.appearance().tintColor = UIColor.white
        self.navigationController?.navigationBar.barTintColor = UIColor(red: 119/255.0, green:186/255.0, blue:162/255.0, alpha: 1.0)
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.white]
        self.navigationItem.leftBarButtonItem = UIBarButtonItem()
        self.navigationItem.leftBarButtonItem!.tintColor = UIColor.white
        self.navigationItem.leftBarButtonItem =  UIBarButtonItem(title: "Volver", style:   UIBarButtonItemStyle.plain, target: self, action: #selector(VerEstadoRenovaciónViewControllerSeconds.btn_clickedLeft(_:)))
        
        
        if let font = UIFont(name: "Exo2-Medium", size: 14) {
            self.navigationItem.leftBarButtonItem!.setTitleTextAttributes([NSForegroundColorAttributeName: UIColor.white,NSFontAttributeName: font], for: UIControlState())
        }
        
     
        menuView = BTNavigationDropdownMenu(navigationController: self.navigationController, containerView: self.navigationController!.view, title: "Waitless", items: items as [AnyObject])
        menuView.cellHeight = 50
        menuView.cellBackgroundColor = self.navigationController?.navigationBar.barTintColor
        menuView.cellSelectionColor = UIColor(red: 0.0/255.0, green:160.0/255.0, blue:195.0/255.0, alpha: 1.0)
        menuView.shouldKeepSelectedCellColor = true
        menuView.cellTextLabelColor = UIColor.white
        menuView.cellTextLabelFont = UIFont(name: "Avenir-Heavy", size: 17)
        menuView.cellTextLabelAlignment = .left // .Center // .Right // .Left
        menuView.arrowPadding = 15
        menuView.animationDuration = 0.5
        menuView.maskBackgroundColor = UIColor.black
        menuView.maskBackgroundOpacity = 0.3
        menuView.isUserInteractionEnabled = false
        menuView.isHidden  = true
        menuView.didSelectItemAtIndexHandler = {(indexPath: Int) -> () in
            print("Did select item at index: \(indexPath)")
            
        }
        
        self.navigationItem.titleView = menuView
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func cancelarNumero(_ sender: AnyObject) {
        
        
        if connectedToNetwork() == true{
            print("Connected")
            cancelarAlert()
        }else{
            print("Not connected")
            // Prepare the popup assets
            let title = "WAITLESS"
            let message = "Lo sentimos, no tienes internet"
            let image = UIImage(named: "AppIcon")
            
            // Create the dialog
            let popup = PopupDialog(title: title, message: message, image: image)
            
            
            // Create second button
            let buttonTwo = DefaultButton(title: "Entendido") {
                
            }
            
            
            
            // Add buttons to dialog
            popup.addButtons([buttonTwo,])
            
            // Present dialog
            self.present(popup, animated: true, completion: nil)
        }
        
    }
    
    func cancelarAlert(){
        
        // Prepare the popup assets
        let title = "WAITLESS"
        let message = "¿Estas seguro que deseas cancelar tu número de atención?"
        let image = UIImage(named: "AppIcon")
        
        // Create the dialog
        let popup = PopupDialog(title: title, message: message, image: image)
        
        
        // Create second button
        let buttonTwo = DefaultButton(title: "Si") {
            self.cancelar()
        }
        
        // Create third button
        let buttonThree = DefaultButton(title: "No") {
            
        }
        
        // Add buttons to dialog
        popup.addButtons([buttonTwo, buttonThree])
        
        // Present dialog
        self.present(popup, animated: true, completion: nil)
        
    }
    
    func cancelar(){
        PKHUD.sharedHUD.contentView = PKHUDProgressView()
        PKHUD.sharedHUD.show()
        
        let defaults = UserDefaults.standard
        
        let rut = defaults.value(forKey: "RutUsuario")
        
        let rutFinal:String = String(describing: rut!)
        
        //print("https://www.waitless.cl/queues/1/numbers/" + rutFinal)
        
        
    Alamofire.request("https://www.waitless.cl/queues/"+String(queueID)+"/numbers/" + rutFinal,method: .post, parameters: nil)
        .responseJSON{ response in
        if let JSON = response.result.value {

            print(JSON)
            
            let response                  = JSON as! NSDictionary
            
            let preferences = UserDefaults.standard
            
            let currentLevelKey = "StatusNumeroRenovacion"
            
            let currentLevel = 0
            preferences.set(currentLevel, forKey: currentLevelKey)
            
            //  Save to disk
            let didSave = preferences.synchronize()
            
            if !didSave {
                //  Couldn't save (I've never seen this happen in real world testing)
            }
            
            PKHUD.sharedHUD.hide(afterDelay: 1.0) { progress in
                // Completion Handler
            }
            
            self.dismiss(animated: true, completion: {});
            //self.performSegue(withIdentifier: "TabBar", sender: self)
            
           
        }
            
        }
        
    
    }
    
    func btn_clickedLeft(_ sender: UIBarButtonItem) {
        self.dismiss(animated: true, completion: {});
    }
    
    @IBAction func actualizarVista(_ sender: AnyObject) {
        
        
        if connectedToNetwork() == true{
            print("Connected")
            actualizarCola()
            actualizarNumero()
            actualizarTiempoEspera()
        }else{
            print("Not connected")
            // Prepare the popup assets
            let title = "WAITLESS"
            let message = "Lo sentimos, no tienes internet"
            let image = UIImage(named: "AppIcon")
            
            // Create the dialog
            let popup = PopupDialog(title: title, message: message, image: image)
            
            
            // Create second button
            let buttonTwo = DefaultButton(title: "Entendido") {
                
            }
            
            // Add buttons to dialog
            popup.addButtons([buttonTwo,])
            
            // Present dialog
            self.present(popup, animated: true, completion: nil)

        }
        
    }
    
    
    func actualizarNumero(){
    
        PKHUD.sharedHUD.contentView = PKHUDProgressView()
        PKHUD.sharedHUD.show()
        
        let defaults = UserDefaults.standard
        
        let rut = defaults.value(forKey: "RutUsuario")
        
        let rutFinal:String = String(describing: rut!)
        
        Alamofire.request("https://www.waitless.cl/queues/"+String(queueID)+"/attentions/"+rutFinal,method: .get, parameters: nil)
        .responseJSON { response in
            
            if let JSON = response.result.value {
                print("JSON: \(JSON)")
                
                let response                  = JSON as! NSDictionary
                
                let value:NSString  = response["value"] as! NSString
             
                
                self.MiNumero.text = value as String
                
                PKHUD.sharedHUD.hide(afterDelay: 1.0) { progress in
                    // Completion Handler
                }
                
            }
        }
      
    
    }
    
    func actualizarTiempoEspera(){
        
        
        PKHUD.sharedHUD.contentView = PKHUDProgressView()
        PKHUD.sharedHUD.show()
        
        let defaults = UserDefaults.standard
        
        let rut = defaults.value(forKey: "RutUsuario")
        
        let rutFinal:String = String(describing: rut!)
        
        
        Alamofire.request("https://www.waitless.cl/queues/"+String(queueID)+"/waiting/"+rutFinal,method: .get, parameters: nil)
            .responseJSON { response in
                
                if let JSON = response.result.value {
                    print("JSON: \(JSON)")
                    
                    let response                  = JSON as! NSDictionary
                    
                    
                    if !(response.object(forKey: "time_stimate_rut") is NSString) {
                        
                        print("Wooops! No string")
                        
                        let time_stimate_rut:NSNumber      = (response.object(forKey: "time_stimate_rut") as! NSNumber)
                        self.TiempoEspera.text = String(describing: time_stimate_rut) + " Minutos"
                        
                    } else {
                        
                        let time_stimate_rut:NSString      = (response.object(forKey: "time_stimate_rut") as! NSString)
                        
                        
                        
                        if time_stimate_rut == "No hay numeros"{
                            self.TiempoEspera.text = "No se puede estimar"
                           
                        }else if time_stimate_rut == "Indeterminado" {
                            self.TiempoEspera.text = "No se puede estimar"
                            
                        }else{
                            
                            if  self.MiNumero.text == self.Numero.text {
                                self.TiempoEspera.text =  "Es tu turno! Dirígete al módulo"
                            }else{
                                self.TiempoEspera.text = "No se puede estimar"
                            }
                            
                        }
                        
                    }

                    PKHUD.sharedHUD.hide(afterDelay: 1.0) { progress in
                        // Completion Handler
                    }
                    
                    
                }
        }
        
    }

  
    
    func actualizarCola(){
        
        Alamofire.request("https://www.waitless.cl/queues/"+String(queueID),method: .get, parameters: nil)
            .responseJSON { response in
             
                if let JSON = response.result.value {
                    print("JSON: \(JSON)")
                    
                    let response                  = JSON as! NSDictionary
                    
                    let data:NSDictionary         = response.object(forKey: "data") as! NSDictionary
                    
                    let current_number:NSString         = (data.object(forKey: "current_number") as! NSString)
                    
                    let name:NSString = (data.object(forKey: "name") as! NSString)

                    let last_number:NSString         = (data.object(forKey: "last_number") as! NSString)
                    
                    
                    self.Numero.text = current_number as String
                    self.titulo.font = UIFont(name:"HelveticaNeue-Bold", size: 17.0)
                    self.titulo.textAlignment = .center
                    self.titulo.text = name as String
                    
                }
        }
        
        
        
    }
    
    
}
