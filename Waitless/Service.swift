//
//  User.swift
//  Enfocus
//
//  Created by Diego Villouta Fredes on 3/4/16.
//  Copyright © 2016 Jumpitt Labs. All rights reserved.
//

import RealmSwift

class Service: Object {
    dynamic var name: String = ""
    dynamic var id: Int = 0
}
