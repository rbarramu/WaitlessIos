//
//  PedirNumeroViewController.swift
//  Waitless
//
//  Created by Daniel Romero on 10-07-16.
//  Copyright © 2016 Debak. All rights reserved.
//

import UIKit
import Alamofire
import PKHUD
import PopupDialog
import BTNavigationDropdownMenu

class PedirNumeroRenovaciónViewController: UIViewController {

    @IBOutlet weak var numeroActual: UILabel!
    @IBOutlet weak var siguienteNumero: UILabel!
    @IBOutlet weak var titulo: UILabel!
    @IBOutlet weak var tiempo: UILabel!
    
    var menuView: BTNavigationDropdownMenu!
    var queueID: Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        menuTab()
       
      
        if connectedToNetwork() == true{
            print("Connected")
            
            actualizar()
            actualizarTiempoEspera()
            
        }else{
            print("Not connected")
            // Prepare the popup assets
            let title = "WAITLESS"
            let message = "Lo sentimos, no tienes internet"
            let image = UIImage(named: "AppIcon")
            
            // Create the dialog
            let popup = PopupDialog(title: title, message: message, image: image)
            
            
            // Create second button
            let buttonTwo = DefaultButton(title: "Entendido") {
                
            }
            
            // Add buttons to dialog
            popup.addButtons([buttonTwo,])
            
            // Present dialog
            self.present(popup, animated: true, completion: nil)
            
        }
        
        
       
        // Do any additional setup after loading the view.
    }

    override func viewWillAppear(_ animated: Bool) {
            self.tabBarController?.tabBar.isHidden = true
    }
    
    func menuTab(){
        let items = [String]()
        self.navigationController?.navigationBar.isTranslucent = false
        UINavigationBar.appearance().tintColor = UIColor.white
        self.navigationController?.navigationBar.barTintColor = UIColor(red: 119/255.0, green:186/255.0, blue:162/255.0, alpha: 1.0)
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.white]
        self.navigationItem.leftBarButtonItem = UIBarButtonItem()
        self.navigationItem.leftBarButtonItem!.tintColor = UIColor.white
        self.navigationItem.leftBarButtonItem =  UIBarButtonItem(title: "Volver", style:   UIBarButtonItemStyle.plain, target: self, action: #selector(PedirNumeroRenovaciónViewController.btn_clickedLeft(_:)))
    
        
        if let font = UIFont(name: "Exo2-Medium", size: 14) {
            self.navigationItem.leftBarButtonItem!.setTitleTextAttributes([NSForegroundColorAttributeName: UIColor.white,NSFontAttributeName: font], for: UIControlState())
        }
        
        menuView = BTNavigationDropdownMenu(navigationController: self.navigationController, containerView: self.navigationController!.view, title: "", items: items as [AnyObject])
        menuView.cellHeight = 50
        menuView.cellBackgroundColor = self.navigationController?.navigationBar.barTintColor
        menuView.cellSelectionColor = UIColor(red: 0.0/255.0, green:160.0/255.0, blue:195.0/255.0, alpha: 1.0)
        menuView.shouldKeepSelectedCellColor = true
        menuView.cellTextLabelColor = UIColor.white
        menuView.cellTextLabelFont = UIFont(name: "Avenir-Heavy", size: 17)
        menuView.cellTextLabelAlignment = .left // .Center // .Right // .Left
        menuView.arrowPadding = 15
        menuView.animationDuration = 0.5
        menuView.maskBackgroundColor = UIColor.black
        menuView.maskBackgroundOpacity = 0.3
        menuView.isUserInteractionEnabled = false
        menuView.isHidden  = true
        menuView.didSelectItemAtIndexHandler = {(indexPath: Int) -> () in
            print("Did select item at index: \(indexPath)")
          
        }
        
        self.navigationItem.titleView = menuView
    }
    
   
    
    func btn_clickedLeft(_ sender: UIBarButtonItem) {
        self.dismiss(animated: true, completion: {});
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
 
 
    func actualizar(){
        
        
        PKHUD.sharedHUD.contentView = PKHUDProgressView()
        PKHUD.sharedHUD.show()
        
        
        Alamofire.request("https://www.waitless.cl/queues/"+String(queueID),method: .get, parameters: nil)
            .responseJSON { response in
               
                
                if let JSON = response.result.value {
                    print("JSON: \(JSON)")
                    
                    let response                  = JSON as! NSDictionary
                    
                    let data:NSDictionary         = response.object(forKey: "data") as! NSDictionary
                    
                    let current_number:NSString         = (data.object(forKey: "current_number") as! NSString)
                    
                    let last_number:NSString         = (data.object(forKey: "last_number") as! NSString)
                    
                    let next_number:NSInteger  = Int(last_number as String)! + 1
                    
                    let name:NSString = (data.object(forKey: "name") as! NSString)

                    PKHUD.sharedHUD.hide(afterDelay: 1.0) { progress in
                        // Completion Handler
                    }
                    
                    self.numeroActual.text = current_number as String
                    self.siguienteNumero.text = String(next_number)
                    self.titulo.font = UIFont(name:"HelveticaNeue-Bold", size: 17.0)
                    self.titulo.textAlignment = .center
                    self.titulo.text = name as String
                }
        }
    }
    
    @IBAction func actualizarView(_ sender: AnyObject) {
        
        actualizar()
        actualizarTiempoEspera()
    }
    
    //////////////// actualizarTiempoEspera /////////////////////////////////////////////
    func actualizarTiempoEspera(){
        
        
        PKHUD.sharedHUD.contentView = PKHUDProgressView()
        PKHUD.sharedHUD.show()
        
        let defaults = UserDefaults.standard
        
        let rut = defaults.value(forKey: "RutUsuario")
        
        let rutFinal:String = String(describing: rut!)
        
        
        Alamofire.request("https://www.waitless.cl/queues/"+String(queueID)+"/waiting/"+rutFinal,method: .get, parameters: nil)
            .responseJSON { response in
                
                if let JSON = response.result.value {
                    print("JSON: \(JSON)")
                    
                    let response                  = JSON as! NSDictionary
                    
                    if !(response.object(forKey: "time_stimate_rut")  is NSString) {
                        
                        print("Wooops! No string")
                        
                        let time_stimate_rut:NSNumber      = (response.object(forKey: "time_stimate_rut") as! NSNumber)
                        self.tiempo.text = String(describing: time_stimate_rut) + " Minutos"
                        
                    } else {
                        
                        let time_stimate_rut:NSString      = (response.object(forKey: "time_stimate_rut") as! NSString)
                        
                        if time_stimate_rut == "No hay numeros"{
                            self.tiempo.text = "No se puede estimar"
                            
                        }else if time_stimate_rut == "Indeterminado" {
                            self.tiempo.text = "No se puede estimar"
                            
                        }else{
                            self.tiempo.text = "¡Ya fuiste atendido!"
                        }
                        
                    }
                    
                    PKHUD.sharedHUD.hide(afterDelay: 1.0) { progress in
                        // Completion Handler
                    }
                    
                    
                    
                }
        }
        
    }

  

     //////////////// TomarNumero /////////////////////////////////////////////
    @IBAction func TomarNumero(_ sender: AnyObject) {
        
        
        if connectedToNetwork() == true{
            print("Connected")
            PedirNumero()
        }else{
            print("Not connected")
            // Prepare the popup assets
            let title = "WAITLESS"
            let message = "Lo sentimos, no tienes internet"
            let image = UIImage(named: "AppIcon")
            
            // Create the dialog
            let popup = PopupDialog(title: title, message: message, image: image)
            
            
            // Create second button
            let buttonTwo = DefaultButton(title: "Entendido") {
                
            }
            
            
            
            // Add buttons to dialog
            popup.addButtons([buttonTwo,])
            
            // Present dialog
            self.present(popup, animated: true, completion: nil)
        }
        
        
    }
   
    func PedirNumero(){
     
        PKHUD.sharedHUD.contentView = PKHUDProgressView()
        PKHUD.sharedHUD.show()
        
        let defaults = UserDefaults.standard
        
        let rut = defaults.value(forKey: "RutUsuario")
        
        let rutFinal:String = String(describing: rut!)
        
      
        Alamofire.request("https://www.waitless.cl/queues/"+String(queueID)+"/attentions/" + rutFinal + "/iOS",method: .post, parameters: nil)
            .responseJSON { response in
              
                if let JSON = response.result.value {
                  
                    print("JSON: \(JSON)")
                    let response                  = JSON as! NSDictionary
                    
                    
                    let data:NSDictionary         = response.object(forKey: "data") as! NSDictionary
                    let queue:NSDictionary        = data.object(forKey: "queue") as! NSDictionary
                    let last_number:NSInteger         = (queue.object(forKey: "last_number") as! NSInteger)
                    
                    let preferences = UserDefaults.standard
                    
                    let currentLevelKey = "NumeroPedidoRenovacion"
                    let currentLevel = last_number
                    preferences.set(currentLevel, forKey: currentLevelKey)
                    
                    //  Save to disk
                    let didSave = preferences.synchronize()
                 
                        PKHUD.sharedHUD.hide(afterDelay: 1.0) { progress in
                            // Completion Handler
                        }
                    
                        self.performSegue(withIdentifier: "lastView", sender: self)
                    
                    if !didSave {
                        //  Couldn't save (I've never seen this happen in real world testing)
                    }

                    
                }
        }
        
        
    }


}
