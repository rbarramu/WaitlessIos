//
//  AssessmentListTableViewController.swift
//  Waitless
//
//  Created by Daniel Romero on 10/24/16.
//  Copyright © 2016 Debak. All rights reserved.
//

import UIKit
import BTNavigationDropdownMenu
import Alamofire
import RealmSwift
import PopupDialog
import PKHUD

class AssessmentListTableViewController: UITableViewController {
    
    
    var Colas :[String]! = []
    var ColasSub :[String]! = []
    var ides:[String]! = []
    var status:[String]! = []
    var statusMenu : Int = 0
    var companyName: String? = ""
    var branchID : Int = 0
    
    var menuView: BTNavigationDropdownMenu!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.tableFooterView = UIView()
        menuTab()
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        if connectedToNetwork() == true{
            print("Connected")
            menuTab()
            getQeues()

        }else{
            print("Not connected")
            // Prepare the popup assets
            let title = "WAITLESS"
            let message = "Lo sentimos, no tienes internet"
            let image = UIImage(named: "AppIcon")
            
            // Create the dialog
            let popup = PopupDialog(title: title, message: message, image: image)
            
            
            // Create second button
            let buttonTwo = DefaultButton(title: "Entendido") {
                
            }
            
            
            
            // Add buttons to dialog
            popup.addButtons([buttonTwo,])
            
            // Present dialog
            self.present(popup, animated: true, completion: nil)
        }
    }
    
    func menuTab(){
        let items = [""]
        
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.navigationBar.barTintColor = UIColor(red: 119/255.0, green:186/255.0, blue:162/255.0, alpha: 1.0)
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.white]
        
        menuView = BTNavigationDropdownMenu(navigationController: self.navigationController, containerView: self.navigationController!.view, title: "Waitless", items: items as [AnyObject])
        menuView.cellHeight = 50
        menuView.cellBackgroundColor = self.navigationController?.navigationBar.barTintColor
        menuView.cellSelectionColor = UIColor(red: 0.0/255.0, green:160.0/255.0, blue:195.0/255.0, alpha: 1.0)
        menuView.shouldKeepSelectedCellColor = true
        menuView.cellTextLabelColor = UIColor.white
        menuView.cellTextLabelFont = UIFont(name: "Avenir-Heavy", size: 17)
        menuView.cellTextLabelAlignment = .left // .Center // .Right // .Left
        menuView.arrowPadding = 15
        menuView.animationDuration = 0.5
        menuView.maskBackgroundColor = UIColor.black
        menuView.maskBackgroundOpacity = 0.3
        menuView.isUserInteractionEnabled = false
        menuView.arrowImage =  UIImage(named: "iconReloj")
        menuView.didSelectItemAtIndexHandler = {(indexPath: Int) -> () in
          
        }
        
        self.navigationItem.titleView = menuView
    }
    
    
    
    // MARK: - UITableViewDataSource
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Colas.count
        
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "QRcell", for: indexPath)
        
        let titleName = Colas[indexPath.row]
        cell.textLabel?.text = titleName
        
        let subtitleName = ColasSub[indexPath.row]
        cell.detailTextLabel?.text = subtitleName

        let image: UIImage = UIImage(named: "Image-2")!
        let size = CGSize(width: 30, height: 30)
        cell.imageView?.image = image.imageResize(sizeChange: size)
        cell.selectionStyle = UITableViewCellSelectionStyle.none
        
        return cell
    }
    
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let indexPath = tableView.indexPathForSelectedRow
        
        self.branchID = Int(self.ides[(indexPath?.row)!])!
        
        //print(currentCell.textLabel!.text)
        
        showCustomDialog(id: self.branchID)
    }
    
    func showCustomDialog(id: Int) {
        
    
        // Create a custom view controller
        let ratingVC = RatingViewController(nibName: "RatingViewController", bundle: nil)
        
        // Create the dialog
        let popup = PopupDialog(viewController: ratingVC, buttonAlignment: .horizontal, transitionStyle: .bounceDown, gestureDismissal: true)
        
        // Create first button
        let buttonOne = CancelButton(title: "CANCELAR") {
            //self.label.text = "You canceled the rating dialog"
        }
        
        // Create second button
        let buttonTwo = DefaultButton(title: "EVALUAR") {
            //self.label.text = "You rated \(ratingVC.cosmosStarRating.rating) stars"
            //print("You rated \(ratingVC.cosmosStarRating.rating) stars")
            //print(Int(ratingVC.cosmosStarRating.rating))
            self.rateQeue(id: id,rate: Int(ratingVC.cosmosStarRating.rating))
        }
        
        // Add buttons to dialog
        popup.addButtons([buttonOne, buttonTwo])
        
        // Present dialog
        present(popup, animated: true, completion: nil)
    }
    
    
    func rateQeue(id: Int , rate: Int){
        
        print("Evaluar cola id:\(id) con rate:\(rate)")
        
        PKHUD.sharedHUD.contentView = PKHUDProgressView()
        PKHUD.sharedHUD.show()
       
        Alamofire.request("http://www.waitless.cl/attention/"+String(id)+"/rate/"+String(rate),method: .post, parameters: nil)
            .responseJSON { response in
                
                if let JSON = response.result.value {
                    //print("JSON: \(JSON)")
                    
                    PKHUD.sharedHUD.hide(afterDelay: 1.0) { progress in
                        self.getQeues()
                    }
                
                }
                
        }
        
        
        
    }
    
    
    func getQeues(){
        
        PKHUD.sharedHUD.contentView = PKHUDProgressView()
        PKHUD.sharedHUD.show()
        
        self.status.removeAll(keepingCapacity: false)
        self.ides.removeAll(keepingCapacity: false)
        self.Colas.removeAll(keepingCapacity: false)
        self.ColasSub.removeAll(keepingCapacity: false)
        
        let defaults = UserDefaults.standard
        
        let rut = defaults.value(forKey: "RutUsuario")
        
        let rutFinal:String = String(describing: rut!)
        
        Alamofire.request("http://www.waitless.cl/attentions/"+(rutFinal as String),method: .get, parameters: nil)
            .responseJSON { response in
                
                if let JSON = response.result.value {
                    //print("JSON: \(JSON)")
                    
                    let response                  = JSON as! NSDictionary
                    let data:NSArray  = response["data"] as! NSArray
                    
                    let queues:NSArray  = response["queues"] as! NSArray
                    
                    let branches:NSArray  = response["branches"] as! NSArray
                    
                    if data.count > 0 {
                        
                        for index in 0...(data.count-1) {
                            var rate:Bool
                            
                            if !((data[index] as AnyObject).object(forKey: "rate") is NSNull) {
                                rate = true
                            } else {
                                rate = false
                            }
                            
                            var statusQueue:Bool
                            let status = (data[index] as AnyObject).object(forKey: "status") as! NSString
                            
                            if status == "ready"{
                                statusQueue = false
                            }else{
                                statusQueue = true
                            }
                            
                            if statusQueue == false && rate == false{
                                
                                let queue_id = (data[index] as AnyObject).object(forKey: "queue_id") as! NSString
                                
                                let id = (data[index] as AnyObject).object(forKey: "id") as! Int
                                
                                self.ides.append(String(id))
                                
                                self.status.append(status as String)
                                
                                
                                var name:NSString = ""
                                
                                for index in 0...(queues.count-1) {
                                    
                                    if (queues[index] as AnyObject).object(forKey: "id") as! NSInteger == Int(queue_id as String)!{
                                        
                                        name = (queues[index] as AnyObject).object(forKey: "name") as! NSString
                                        
                                    }
                                }
                                
                                
                                for index in 0...(branches.count-1) {
                                    
                                    if (branches[index] as AnyObject).object(forKey: "id") as! NSInteger == Int(queue_id as String)!{
                                        
                                        let nameCompany = (branches[index] as AnyObject).object(forKey: "name") as! NSString
                                        
                                        self.ColasSub.append("\(nameCompany)")
                                        
                                        let company_id = (branches[index] as AnyObject).object(forKey: "company_id") as! NSString
                                        let idInt = Int(company_id as String)!
                                        
                                        // Get the default Realm
                                        let realm = try! Realm()
                                        
                                        let company = realm.objects(Company.self).filter("id == %@",idInt)
                                        self.companyName = company.first?.name
                                        self.Colas.append("\(name as String)\n\(self.companyName!)")
                                        
                                    }
                                }
                                

                                
                            }else{
                                print("Aun no puede ser evaluada")
                            }
                            
                        }
                    
                    }else {
                        print("Ooops, it's empty")
                        
                    }
                    
                    PKHUD.sharedHUD.hide(afterDelay: 1.0) { progress in
                        self.tableView.reloadData()
                    }
                    
                    
                }
        }
        
        
    }
    
 

    
}
