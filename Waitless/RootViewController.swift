//
//  RootViewController.swift
//  Waitless
//
//  Created by Rocio Barramuño on 24-09-16.
//  Copyright © 2016 Debak. All rights reserved.
//

import UIKit

class RootViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
  
        let preferences = UserDefaults.standard
        
        let currentLevelKey = "RutUsuario"
        
        if preferences.object(forKey: currentLevelKey) == nil {
            //  Doesn't exist
            
            let storyBoard : UIStoryboard = UIStoryboard(name: "Tutorial", bundle:nil)
            let mainView = storyBoard.instantiateViewController(withIdentifier: "Tutorial") as! TutorialViewController
            
            self.present(mainView, animated:true, completion:nil)
            
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.window?.rootViewController = mainView
            
            
        } else {
            
            let storyBoard : UIStoryboard = UIStoryboard(name: "TabBar", bundle:nil)
            let mainView = storyBoard.instantiateViewController(withIdentifier: "TabBar") as! UITabBarController
            self.present(mainView, animated:true, completion:nil)
            
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.window?.rootViewController = mainView
            
        }
  
        
        // Do any additional setup after loading the view.
    }

    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
       
    }

   
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
