//
//  Branches.swift
//  Waitless
//
//  Created by Daniel Romero on 10/31/16.
//  Copyright © 2016 Debak. All rights reserved.
//

import RealmSwift

class Branches: Object {
    dynamic var name: String = ""
    dynamic var id: Int = 0
    dynamic var company_id: String = ""
    dynamic var address: String = ""
    dynamic var lat: String = ""
    dynamic var lon: String = ""
}
