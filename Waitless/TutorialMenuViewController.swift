//
//  TutorialMenuViewController.swift
//  Waitless
//
//  Created by Daniel Romero on 9/30/16.
//  Copyright © 2016 Debak. All rights reserved.
//

import UIKit
import paper_onboarding

class TutorialMenuViewController: UIViewController {

    @IBOutlet weak var onboarding: PaperOnboarding!
    @IBOutlet weak var skipButton: UIButton!
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        
        
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        skipButton.isHidden = false
        
        // EXAMPLE USE FROM CODE
        
        //    let onboarding = PaperOnboarding(itemsCount: 3)
        //    onboarding.dataSource = self
        //    onboarding.translatesAutoresizingMaskIntoConstraints = false
        //    view.addSubview(onboarding)
        //
        //    // add constraints
        //    for attribute: NSLayoutAttribute in [.Left, .Right, .Top, .Bottom] {
        //      let constraint = NSLayoutConstraint(item: onboarding,
        //                                          attribute: attribute,
        //                                          relatedBy: .Equal,
        //                                          toItem: view,
        //                                          attribute: attribute,
        //                                          multiplier: 1,
        //                                          constant: 0)
        //      view.addConstraint(constraint)
        //    }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func buttnoHandler(_ sender: AnyObject) {
    }
}

// MARK: Actions

extension TutorialMenuViewController {
    
    @IBAction func buttonHandler(_ sender: AnyObject) {
        print("skip")
        self.dismiss(animated: true, completion: {});
    }
}

extension TutorialMenuViewController: PaperOnboardingDelegate {
    
    func onboardingWillTransitonToIndex(_ index: Int) {
        //skipButton.isHidden = index == 2 ? false : true
        skipButton.isHidden = false
    }
    
    func onboardingDidTransitonToIndex(_ index: Int) {
        
    }
    
    func onboardingConfigurationItem(_ item: OnboardingContentViewItem, index: Int) {
        
        //    item.titleLabel?.backgroundColor = .redColor()
        //    item.descriptionLabel?.backgroundColor = .redColor()
        //    item.imageView = ...
    }
}

// MARK: PaperOnboardingDataSource

extension TutorialMenuViewController: PaperOnboardingDataSource {
    
    func onboardingItemAtIndex(_ index: Int) -> OnboardingItemInfo {
        let titleFont = UIFont(name: "Nunito-Bold", size: 36.0) ?? UIFont.boldSystemFont(ofSize: 36.0)
        let descriptionFont = UIFont(name: "OpenSans-Regular", size: 14.0) ?? UIFont.systemFont(ofSize: 14.0)
        return [
            (UIImage.Asset.TutorialImage.rawValue, "Waitless", "¡Bienvenido a la aplicación de Waitless, te ayudaremos con el proceso de Renovación de Beneficios 2017!", UIImage.Asset.TutorialImage.rawValue, UIColor(red:0.61, green:0.56, blue:0.74, alpha:1.00), UIColor.white, UIColor.white, titleFont,descriptionFont),
            (UIImage.Asset.RutIcon.rawValue, "¡Así de fácil!", "Para comenzar, solo ingresa tu rut sin puntos ni guión", UIImage.Asset.TutorialImage.rawValue, UIColor(red:0.40, green:0.69, blue:0.71, alpha:1.00), UIColor.white, UIColor.white, titleFont,descriptionFont),
            (UIImage.Asset.FilaIcon.rawValue, "Pide tu número", "Podrás tomar un número y revisar en todo momento el estado que tiene la fila", UIImage.Asset.TutorialImage.rawValue, UIColor(red:92/255, green:92/255, blue:92/255, alpha:1.00), UIColor.white, UIColor.white, titleFont,descriptionFont),
            (UIImage.Asset.TipsIcon.rawValue, "Tips", "Recuerda no dejarlo a última hora, lleva tus papeles los primeros días y te evitarás molestias. ", UIImage.Asset.TutorialImage.rawValue, UIColor(red:0.40, green:0.56, blue:0.71, alpha:1.00), UIColor.white, UIColor.white, titleFont,descriptionFont),
            ][index]
    }
    
    func onboardingItemsCount() -> Int {
        return 4
    }
}
