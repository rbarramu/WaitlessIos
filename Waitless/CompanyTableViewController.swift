//
//  CompanyTableViewController.swift
//  Waitless
//
//  Created by Daniel Romero on 11/5/16.
//  Copyright © 2016 Debak. All rights reserved.
//

import UIKit
import RealmSwift
import BTNavigationDropdownMenu

class CompanyTableViewController: UITableViewController {

    var idService: Int!
    var names = [String]()
    var idcompany = [Int]()
    var images:[String]! = []
    var branchID : Int = 0
    var menuView: BTNavigationDropdownMenu!
  
    override func viewDidLoad() {
        super.viewDidLoad()

        
        refreshTable()
        tableView.tableFooterView = UIView()
    }

    func refreshTable(){
        // Get the default Realm
        let realm = try! Realm()
        
        let companies = realm.objects(Company.self).filter("service == %@",String(idService))
        
        
        if companies.count > 0 {
            
            
            for index in 0...(companies.count-1){
                
                names.append(companies[index].name)
                images.append(companies[index].imgURL)
                idcompany.append(companies[index].id)
            }
            
        }

        tableView.reloadData()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        menuTab()
    }
    
    func menuTab(){
        let items = [""]
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.navigationBar.barTintColor = UIColor(red: 119/255.0, green:186/255.0, blue:162/255.0, alpha: 1.0)
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.white]
        
        menuView = BTNavigationDropdownMenu(navigationController: self.navigationController, containerView: self.navigationController!.view, title: "Selecciona una empresa", items: items as [AnyObject])
        menuView.cellHeight = 50
        menuView.cellBackgroundColor = self.navigationController?.navigationBar.barTintColor
        menuView.cellSelectionColor = UIColor(red: 0.0/255.0, green:160.0/255.0, blue:195.0/255.0, alpha: 1.0)
        menuView.isUserInteractionEnabled = false
        menuView.shouldKeepSelectedCellColor = true
        menuView.cellTextLabelColor = UIColor.white
        menuView.cellTextLabelFont = UIFont(name: "Avenir-Heavy", size: 17)
        menuView.cellTextLabelAlignment = .left // .Center // .Right // .Left
        menuView.arrowPadding = 15
        menuView.animationDuration = 0.5
        menuView.maskBackgroundColor = UIColor.black
        menuView.maskBackgroundOpacity = 0.3
        menuView.arrowImage =  UIImage(named: "iconReloj")
        menuView.didSelectItemAtIndexHandler = {(indexPath: Int) -> () in
            //print("Did select item at index: \(indexPath)")
            
            
        }
        
        self.navigationItem.titleView = menuView
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
       
        return names.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath)
        
        let titleName = names[indexPath.row]
        cell.textLabel?.text = titleName
        
        //let subtitleName = ColasSub[indexPath.row]
        cell.detailTextLabel?.text = ""
        
        if let url = NSURL(string: images[indexPath.row]){
            let data = NSData(contentsOf: url as URL)
            let image = UIImage(data: data! as Data)
            let size = CGSize(width: 30, height: 30)
            cell.imageView?.image = image?.imageResize(sizeChange: size)
        }
        
        cell.selectionStyle = UITableViewCellSelectionStyle.none
        return cell

    }
    

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let indexPath = tableView.indexPathForSelectedRow
        
        self.branchID = Int(self.idcompany[(indexPath?.row)!])
        
      
        // Define identifier
        let notificationName = Notification.Name("NotificationIdentifier")
        NotificationCenter.default.post(name: notificationName, object: nil, userInfo:["branchID":String(branchID)])
      

        self.dismiss(animated: true, completion: nil)
        
    }

}
