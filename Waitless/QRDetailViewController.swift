//
//  QRDetailViewController.swift
//  Waitless
//
//  Created by Daniel Romero on 10/24/16.
//  Copyright © 2016 Debak. All rights reserved.
//

import UIKit
import BTNavigationDropdownMenu
import Alamofire
import RealmSwift
import PopupDialog
import PKHUD
import QRCode

class QRDetailViewController: UIViewController {

    @IBOutlet weak var imageQR: UIImageView!
    @IBOutlet weak var nameQueue: UILabel!
    @IBOutlet weak var company: UILabel!
    @IBOutlet weak var branch: UILabel!
    @IBOutlet weak var myNumber: UILabel!
    
    var menuView: BTNavigationDropdownMenu!
    var queueID: Int = 0
    var dataID: Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
        setupNavigationBar()
        
        if connectedToNetwork() == true{
            print("Connected")
            actualizarNumero()
            refreshView()
            qrload()
       
        }else{
            print("Not connected")
            // Prepare the popup assets
            let title = "WAITLESS"
            let message = "Lo sentimos, no tienes internet"
            let image = UIImage(named: "AppIcon")
            
            // Create the dialog
            let popup = PopupDialog(title: title, message: message, image: image)
            
            
            // Create second button
            let buttonTwo = DefaultButton(title: "Entendido") {
                
            }
            
            
            
            // Add buttons to dialog
            popup.addButtons([buttonTwo,])
            
            // Present dialog
            self.present(popup, animated: true, completion: nil)
        }
        
        
       
               //let qrCode = QRCode(String(queueID))!
        //let image = qrCode.image
        menuTab()
        //imageQR.image = image
        
        // Do any additional setup after loading the view.
    }

    
    func refreshView(){
        
        // Get the default Realm
        let realm = try! Realm()
        
        let queues = realm.objects(Queues.self).filter("id == %@",queueID)
        
        if queues.count > 0 {
            nameQueue.text = queues.first?.name
           
            let branches = realm.objects(Branches.self).filter("id == %@",Int(queues.first!.id_branch)!)
           
            if branches.count > 0 {
                branch.text = branches.first?.name
                
                let companys = realm.objects(Company.self).filter("id == %@",Int(branches.first!.company_id)!)
                if companys.count > 0 {
                    company.text = companys.first?.name
                }
            }
            
        }
        
    }

    
    func menuTab(){
        let items = [String]()
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.navigationBar.barTintColor = UIColor(red: 119/255.0, green:186/255.0, blue:162/255.0, alpha: 1.0)
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.white]
        
        menuView = BTNavigationDropdownMenu(navigationController: self.navigationController, containerView: self.navigationController!.view, title: "Waitless", items: items as [AnyObject])
        menuView.cellHeight = 50
        menuView.cellBackgroundColor = self.navigationController?.navigationBar.barTintColor
        menuView.cellSelectionColor = UIColor(red: 0.0/255.0, green:160.0/255.0, blue:195.0/255.0, alpha: 1.0)
        menuView.isHidden = true
        menuView.shouldKeepSelectedCellColor = true
        menuView.cellTextLabelColor = UIColor.white
        menuView.cellTextLabelFont = UIFont(name: "Avenir-Heavy", size: 17)
        menuView.cellTextLabelAlignment = .left // .Center // .Right // .Left
        menuView.arrowPadding = 15
        menuView.animationDuration = 0.5
        menuView.maskBackgroundColor = UIColor.black
        menuView.maskBackgroundOpacity = 0.3
        
        self.navigationItem.titleView = menuView
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setupNavigationBar() {
        
        UINavigationBar.appearance().tintColor = UIColor.white
        UINavigationBar.appearance().setBackgroundImage(UIImage(), for: .default)
        // Sets shadow (line below the bar) to a blank image
        UINavigationBar.appearance().shadowImage = UIImage()
        // Sets the translucent background color
        UINavigationBar.appearance().backgroundColor = UIColor(red: 0.0/255.0, green:180/255.0, blue:220/255.0, alpha: 1.0)

        
        // Set translucent. (Default value is already true, so this can be removed if desired.)
        UINavigationBar.appearance().isTranslucent = true
        if let font = UIFont(name: "Exo2-Bold", size: 14) {
            self.navigationController?.navigationBar.titleTextAttributes =
                [NSForegroundColorAttributeName: UIColor.white,
                 NSFontAttributeName: font]
        }
        
        self.navigationItem.leftBarButtonItem = UIBarButtonItem()
        self.navigationItem.leftBarButtonItem!.tintColor = UIColor.white
        self.navigationItem.leftBarButtonItem =  UIBarButtonItem(title: "Volver", style:   UIBarButtonItemStyle.plain, target: self, action: #selector(QRDetailViewController.btn_clickedLeft(_:)))
        if let font = UIFont(name: "Exo2-Medium", size: 14) {
            self.navigationItem.leftBarButtonItem!.setTitleTextAttributes([NSFontAttributeName: font], for: UIControlState())
        }
        
    }
    
    func btn_clickedLeft(_ sender: UIBarButtonItem) {
        self.dismiss(animated: true, completion: {});
    }

    func qrload(){
        
        PKHUD.sharedHUD.contentView = PKHUDProgressView()
        PKHUD.sharedHUD.show()
        
        let defaults = UserDefaults.standard
        
        let rut = defaults.value(forKey: "RutUsuario")
        
      
        let rutFinal:String = String(describing: rut!)
        print("https://www.waitless.cl/png/rut/"+rutFinal+"/attention/"+String(queueID))
        
        Alamofire.request("https://www.waitless.cl/png/rut/"+rutFinal+"/attention/"+String(dataID),method: .get, parameters: nil)
            .responseJSON { response in
                
                if let JSON = response.result.value {
                    print("JSON: \(JSON)")
                    
                    let response                  = JSON as! NSDictionary
                    
                    let qrImage:NSString  = response["url"] as! NSString
                    
                    if let url = NSURL(string: qrImage as String){
                        let data = NSData(contentsOf: url as URL)
                        let image = UIImage(data: data! as Data)
                        self.imageQR.image = image
                    }
                    
                    PKHUD.sharedHUD.hide(afterDelay: 1.0) { progress in
                        // Completion Handler
                    }
                    
                }
        }
        
    }
    
    
    func actualizarNumero(){
        
        PKHUD.sharedHUD.contentView = PKHUDProgressView()
        PKHUD.sharedHUD.show()
        
        let defaults = UserDefaults.standard
        
        let rut = defaults.value(forKey: "RutUsuario")
        
        let rutFinal:String = String(describing: rut!)
        
        Alamofire.request("https://www.waitless.cl/queues/"+String(queueID)+"/attentions/"+rutFinal,method: .get, parameters: nil)
            .responseJSON { response in
                
                if let JSON = response.result.value {
                    //print("JSON: \(JSON)")
                    
                    let response                  = JSON as! NSDictionary
                    
                    let value:NSString  = response["value"] as! NSString
                    
                    
                    self.myNumber.text = value as String
                    
                    PKHUD.sharedHUD.hide(afterDelay: 1.0) { progress in
                        // Completion Handler
                    }
                    
                }
        }
        
        
    }
}
