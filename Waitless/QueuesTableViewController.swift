//
//  QueuesTableViewController.swift
//  Waitless
//
//  Created by Daniel Romero on 11/5/16.
//  Copyright © 2016 Debak. All rights reserved.
//

import UIKit
import BTNavigationDropdownMenu
import Alamofire
import PKHUD
import PopupDialog
import RealmSwift
import Kingfisher

class QueuesTableViewController: UITableViewController {

    var branchID: Int!
    var names = [String]()
    var idqueue = [Int]()
    var queueID : Int = 0
    var menuView: BTNavigationDropdownMenu!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        refreshTable()
        menuTab()
        tableView.tableFooterView = UIView()
        
    }

    func refreshTable(){
    
        // Get the default Realm
        let realm = try! Realm()
        
        let queues = realm.objects(Queues.self).filter("id_branch == %@",String(branchID))
        
        
        if queues.count > 0 {
            
            
            for index in 0...(queues.count-1){
                
                names.append(queues[index].name)
                idqueue.append(queues[index].id)
            }
            
        }
        
        tableView.reloadData()
    
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    
    func menuTab(){
        let items = [""]
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.navigationBar.barTintColor = UIColor(red: 119/255.0, green:186/255.0, blue:162/255.0, alpha: 1.0)
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.white]
        
        menuView = BTNavigationDropdownMenu(navigationController: self.navigationController, containerView: self.navigationController!.view, title: "Selecciona una cola", items: items as [AnyObject])
        menuView.cellHeight = 50
        menuView.cellBackgroundColor = self.navigationController?.navigationBar.barTintColor
        menuView.cellSelectionColor = UIColor(red: 0.0/255.0, green:160.0/255.0, blue:195.0/255.0, alpha: 1.0)
        menuView.isUserInteractionEnabled = false
        menuView.shouldKeepSelectedCellColor = true
        menuView.cellTextLabelColor = UIColor.white
        menuView.cellTextLabelFont = UIFont(name: "Avenir-Heavy", size: 17)
        menuView.cellTextLabelAlignment = .left // .Center // .Right // .Left
        menuView.arrowPadding = 15
        menuView.animationDuration = 0.5
        menuView.maskBackgroundColor = UIColor.black
        menuView.maskBackgroundOpacity = 0.3
        menuView.arrowImage =  UIImage(named: "iconReloj")
        menuView.didSelectItemAtIndexHandler = {(indexPath: Int) -> () in
            //print("Did select item at index: \(indexPath)")
            
            
        }
        
        self.navigationItem.leftBarButtonItem = UIBarButtonItem()
        self.navigationItem.leftBarButtonItem =  UIBarButtonItem(title: "Volver", style:   UIBarButtonItemStyle.plain, target: self, action: #selector(QueuesTableViewController.btn_clickedLeft(_:)))
        if let font = UIFont(name: "Exo2-Medium", size: 14) {
            self.navigationItem.leftBarButtonItem!.setTitleTextAttributes([NSFontAttributeName: font], for: UIControlState())
        }

        
        self.navigationItem.titleView = menuView
    }
    
    
    func btn_clickedLeft(_ sender: UIBarButtonItem) {
       self.dismiss(animated: true, completion: {});
    }
    
    
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return names.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath)
        
        let titleName = names[indexPath.row]
        cell.textLabel?.text = titleName
        
        //let subtitleName = ColasSub[indexPath.row]
        cell.detailTextLabel?.text = ""
        
        let image: UIImage = UIImage(named: "Image-2")!
        let size = CGSize(width: 30, height: 30)
        cell.imageView?.image = image.imageResize(sizeChange: size)
        cell.selectionStyle = UITableViewCellSelectionStyle.none
        

        return cell
        
    }
 
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let indexPath = tableView.indexPathForSelectedRow
        
        self.queueID = Int(self.idqueue[(indexPath?.row)!])
        checkStatusQueue()
    }
    
    
    func checkStatusQueue(){
        
        let defaults = UserDefaults.standard
        
        let rut = defaults.value(forKey: "RutUsuario")
        
        let rutFinal:String = String(describing: rut!)
        
        Alamofire.request("https://www.waitless.cl/queues/"+String(queueID)+"/unique/"+(rutFinal as String),method: .get, parameters: nil)
            .responseJSON { response in
                
                if let JSON = response.result.value {
                    print("JSON: \(JSON)")
                    
                    let response                  = JSON as! NSDictionary
                    
                    let status:NSString  = response["status"] as! NSString
                    
                    if status == "No existe"{
                        self.performSegue(withIdentifier: "numberRequest", sender: nil)
                    }else if status == "notattending"{
                        self.performSegue(withIdentifier: "numberRequest", sender: nil)
                    }else if status == "ready"{
                        self.performSegue(withIdentifier: "numberRequest", sender: nil)
                    }else if status == "waiting"{
                        self.performSegue(withIdentifier: "WaitingRequest", sender: nil)
                    }else if status == "attending"{
                        self.alertStatus()
                    }
                    
                }
        }
        
        
    }
    
    
    func alertStatus(){
        // Prepare the popup assets
        let title = "WAITLESS"
        let message = "¡Es tu turno! Dirígete al módulo"
        
        // Create the dialog
        let popup = PopupDialog(title: title, message: message, image: nil)
        
        // Create buttons
        let buttonOne = CancelButton(title: "OK") {
            print("You canceled the car dialog.")
        }
        popup.addButtons([buttonOne])
        
        // Present dialog
        self.present(popup, animated: true, completion: nil)
    }

    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "numberRequest"{
            
            let numberRequest = segue.destination as! UINavigationController
            let numberRe = numberRequest.topViewController as! PedirNumeroRenovaciónViewController
            numberRe.queueID = queueID
            
        }else{
            let waitingRequest = segue.destination as! UINavigationController
            let waitingRe = waitingRequest.topViewController as! VerEstadoRenovaciónViewControllerSeconds
            waitingRe.queueID = queueID
            
        }
    }

}
