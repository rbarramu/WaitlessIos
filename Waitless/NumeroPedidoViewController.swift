//
//  NumeroPedidoViewController.swift
//  Waitless
//
//  Created by Daniel Romero on 11-07-16.
//  Copyright © 2016 Debak. All rights reserved.
//

import UIKit
import BTNavigationDropdownMenu

class NumeroPedidoViewController: UIViewController {

    @IBOutlet weak var numeroAtencion: UILabel!
    
    var menuView: BTNavigationDropdownMenu!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        menuTab()
     
        
        let preferences = UserDefaults.standard
        
        let currentLevelKey = "NumeroPedidoRenovacion"
        
        if preferences.object(forKey: currentLevelKey) == nil {
            //  Doesn't exist
            
        } else {
            let currentLevel = preferences.integer(forKey: currentLevelKey)
            
            print(currentLevel)
            
            numeroAtencion.text = String(currentLevel)
        }

       
        
        // Do any additional setup after loading the view.
    }

    func menuTab(){
        let items = [String]()
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.navigationBar.barTintColor = UIColor(red: 119/255.0, green:186/255.0, blue:162/255.0, alpha: 1.0)
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.white]
        
        self.navigationItem.setHidesBackButton(true, animated:true);
        
        menuView = BTNavigationDropdownMenu(navigationController: self.navigationController, containerView: self.navigationController!.view, title: "Waitless", items: items as [AnyObject])
        menuView.cellHeight = 50
        menuView.cellBackgroundColor = self.navigationController?.navigationBar.barTintColor
        menuView.cellSelectionColor = UIColor(red: 0.0/255.0, green:160.0/255.0, blue:195.0/255.0, alpha: 1.0)
        menuView.shouldKeepSelectedCellColor = true
        menuView.cellTextLabelColor = UIColor.white
        menuView.cellTextLabelFont = UIFont(name: "Avenir-Heavy", size: 17)
        menuView.cellTextLabelAlignment = .left // .Center // .Right // .Left
        menuView.arrowPadding = 15
        menuView.isHidden  = true
        menuView.animationDuration = 0.5
        menuView.maskBackgroundColor = UIColor.black
        menuView.maskBackgroundOpacity = 0.3
        menuView.isUserInteractionEnabled = false
        menuView.didSelectItemAtIndexHandler = {(indexPath: Int) -> () in
            print("Did select item at index: \(indexPath)")
            
        }
        
        self.navigationItem.titleView = menuView
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    @IBAction func backbutton(_ sender: AnyObject) {
        self.dismiss(animated: true, completion: {});
    }
  
   
}
