//
//  MapViewController.swift
//  Waitless
//
//  Created by Daniel Romero on 10/24/16.
//  Copyright © 2016 Debak. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation
import BTNavigationDropdownMenu
import Alamofire
import PKHUD
import PopupDialog
import RealmSwift
import Kingfisher



class MapViewController: UIViewController,CLLocationManagerDelegate, MKMapViewDelegate {

    @IBOutlet weak var mapDetail: MKMapView!
    
    var menuView: BTNavigationDropdownMenu!
    var locationManager: CLLocationManager!
    var services: [String]! = []
    
    var coordinatesLat: [Double]! = []
    var coordinatesLong: [Double]! = []
    var names:[String]! = []
    var ides:[Int]! = []
    var imagesCompany: String? = ""
    var statusMenu: Int = 0
    var idService:Int = 0
    var pickedOSX: String?
    var brancheId: Int = 0
    
    
    
    enum ItemsType : Int {
        case label, customView
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
        
        if connectedToNetwork() == true{
            print("Connected")
            getServices()
        }else{
            print("Not connected")
            // Prepare the popup assets
            let title = "WAITLESS"
            let message = "Lo sentimos, no tienes internet"
            let image = UIImage(named: "AppIcon")
            
            // Create the dialog
            let popup = PopupDialog(title: title, message: message, image: image)
            
            
            // Create second button
            let buttonTwo = DefaultButton(title: "Entendido") {
                
            }
            
            
            
            // Add buttons to dialog
            popup.addButtons([buttonTwo,])
            
            // Present dialog
            self.present(popup, animated: true, completion: nil)
        }
        
        
        locationManager = CLLocationManager()
        locationManager.delegate = self
        mapDetail.delegate = self
        mapDetail.showsUserLocation = true
        //getServices()
        
        
        
        if CLLocationManager.authorizationStatus() == .authorizedWhenInUse {
            locationManager!.startUpdatingLocation()
        } else {
            locationManager!.requestWhenInUseAuthorization()
        }
        
        // Define identifier
        let notificationName = Notification.Name("NotificationIdentifier")
        
        // Register to receive notification
        NotificationCenter.default.addObserver(self, selector: #selector(MapViewController.catchNotification), name: notificationName, object: nil)

     
        // Define identifier
        let notificationNameRefresh = Notification.Name("Refresh")
        
        // Register to receive notification
        NotificationCenter.default.addObserver(self, selector: #selector(MapViewController.refreshNotification), name: notificationNameRefresh, object: nil)
        
    }
   
   
    override func viewDidAppear(_ animated: Bool) {
       
    }
    
    
    func refreshNotification(notification:Notification) -> Void {
        self.services.removeAll(keepingCapacity: true)
        getServices()

    
    }
    
    func catchNotification(notification:Notification) -> Void {
      
        var userInfo = notification.userInfo
        let branchID  = userInfo?["branchID"] as? String
        //print(branchID)
        self.loadPointsMaps(id: branchID!)
    }
    
    func menuTab(){
    
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.navigationBar.barTintColor = UIColor(red: 119/255.0, green:186/255.0, blue:162/255.0, alpha: 1.0)
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.white]
        
        menuView = BTNavigationDropdownMenu(navigationController: self.navigationController, containerView: self.navigationController!.view, title: "Buscar Colas", items: services as [AnyObject])
        menuView.cellHeight = 50
        menuView.cellBackgroundColor = self.navigationController?.navigationBar.barTintColor
        menuView.cellSelectionColor = UIColor(red: 0.0/255.0, green:160.0/255.0, blue:195.0/255.0, alpha: 1.0)
        menuView.shouldKeepSelectedCellColor = true
        menuView.cellTextLabelColor = UIColor.white
        menuView.cellTextLabelFont = UIFont(name: "Avenir-Heavy", size: 17)
        menuView.cellTextLabelAlignment = .left // .Center // .Right // .Left
        menuView.arrowPadding = 15
        menuView.animationDuration = 0.5
        menuView.maskBackgroundColor = UIColor.black
        menuView.maskBackgroundOpacity = 0.3
        menuView.didSelectItemAtIndexHandler = {(indexPath: Int) -> () in
            //print("Did select item at index: \(indexPath)")
            
            
            let allAnnotations = self.mapDetail.annotations
            self.mapDetail.removeAnnotations(allAnnotations)
            self.getServiceName(namePicker: self.services[indexPath])
            
            self.performSegue(withIdentifier: "showCompany", sender: nil)
            
        
        }
        
        self.navigationItem.rightBarButtonItem = UIBarButtonItem()
        self.navigationItem.rightBarButtonItem =  UIBarButtonItem(title: "Ayuda", style:   UIBarButtonItemStyle.plain, target: self, action: #selector(MapViewController.btn_clickedLeft(_:)))
        if let font = UIFont(name: "Exo2-Medium", size: 14) {
            self.navigationItem.leftBarButtonItem!.setTitleTextAttributes([NSFontAttributeName: font], for: UIControlState())
        }

       
        self.navigationItem.titleView = menuView
    }

    func btn_clickedLeft(_ sender: UIBarButtonItem) {
        let refreshAlert = UIAlertController(title: "Waitless", message: "En este mapa, podrás ver las colas que se encuentran disponibles. Para pedir número ingresa a Buscar Colas en el menú superior para comenzar", preferredStyle: UIAlertControllerStyle.alert)
        
        refreshAlert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action: UIAlertAction!) in
            print("Handle Ok logic here")
            
        }))
        
        self.present(refreshAlert, animated: true, completion: nil)
    }



    func getServiceName(namePicker: String){
    
        // Get the default Realm
        let realm = try! Realm()
    
        let services = realm.objects(Service.self).filter("name == %@",namePicker)
    
        if services.count > 0 {
            idService = (services.first?.id)!
            
        }
    }
    

    override func viewWillAppear(_ animated: Bool) {
        menuTab()
    }
    
    
   
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
  
    
    
    // Called when the region displayed by the map view is about to change
    func mapView(_ mapView: MKMapView, regionWillChangeAnimated animated: Bool) {
        //print(#function)
    }
    
    class CustomPointAnnotation: MKPointAnnotation {
        var imageName: String!
    }
    
    // Called when the annotation was added
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        
        //print("generate points")
        if !(annotation is CustomPointAnnotation) {
            return nil
        }
        
        let reuseId = "test"
        
        var anView = mapView.dequeueReusableAnnotationView(withIdentifier: reuseId)
        if anView == nil {
            anView = MKAnnotationView(annotation: annotation, reuseIdentifier: reuseId)
            anView?.canShowCallout = true
            let rightButton: AnyObject! = UIButton(type: UIButtonType.detailDisclosure)
            anView?.rightCalloutAccessoryView = rightButton as? UIView
            anView?.rightCalloutAccessoryView?.isHidden = true
        }
        else {
            anView?.annotation = annotation
        }
        
        let cpa = annotation as! CustomPointAnnotation
       
        if let url = NSURL(string: cpa.imageName){
            let data = NSData(contentsOf: url as URL)
            let image = UIImage(data: data! as Data)
            anView?.image = image
        }
        
        
        return anView
        
    }
    
    func mapView(_ mapView: MKMapView, annotationView view: MKAnnotationView, calloutAccessoryControlTapped control: UIControl) {
        //print(#function)
        if control == view.rightCalloutAccessoryView {
          
            let title = view.annotation?.title!
          
            // Get the default Realm
            let realm = try! Realm()
            let branches = realm.objects(Branches.self).filter("name == %@",title!)
            
            if branches.count > 0 {
                brancheId = (branches.first?.id)!
                //print("brancheId \(brancheId)")
                self.performSegue(withIdentifier: "showQueue", sender: nil)
            }
          
        }
    }
    
    func mapView(_ mapView: MKMapView, annotationView view: MKAnnotationView, didChange newState: MKAnnotationViewDragState, fromOldState oldState: MKAnnotationViewDragState) {
        if newState == MKAnnotationViewDragState.ending {
            let droppedAt = view.annotation?.coordinate
            //print(droppedAt)
        }
    }

   
    
    func loadMap(){
        
        let latitude:CLLocationDegrees = self.locationManager!.location!.coordinate.latitude
        
        let longitude:CLLocationDegrees = self.locationManager!.location!.coordinate.longitude
        
        let latDelta:CLLocationDegrees = 0.05
        
        let lonDelta:CLLocationDegrees = 0.05
        
        let span = MKCoordinateSpanMake(latDelta, lonDelta)
        
        let location = CLLocationCoordinate2DMake(latitude, longitude)
        
        let region = MKCoordinateRegionMake(location, span)
        
        mapDetail.setRegion(region, animated: false)
        
    }
   
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        
        
        switch status {
        case .notDetermined:
            print("NotDetermined")
        case .restricted:
            print("Restricted")
        case .denied:
            print("Denied")
        case .authorizedAlways:
            print("AuthorizedAlways")
        case .authorizedWhenInUse:
            print("AuthorizedWhenInUse")
            loadMap()
            locationManager!.startUpdatingLocation()
        }
    }
    
    
 
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        
        let location = locations.first!
        let coordinateRegion = MKCoordinateRegionMakeWithDistance(location.coordinate, 500, 500)
        mapDetail.setRegion(coordinateRegion, animated: true)
        locationManager?.stopUpdatingLocation()
        locationManager = nil
        
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("Failed to initialize GPS: ", error.localizedDescription)
    }
    
    
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        let renderer = MKPolylineRenderer(overlay: overlay)
        renderer.strokeColor = UIColor.red
        renderer.lineWidth = 4.0
        
        return renderer
    }
    
    
    func getServices(){
        
        PKHUD.sharedHUD.contentView = PKHUDProgressView()
        PKHUD.sharedHUD.show()
    
        
        Alamofire.request("http://www.waitless.cl/services/",method: .get, parameters: nil)
            .responseJSON { response in
                
                if let JSON = response.result.value {
                    //print("JSON: \(JSON)")
                    
                    let response                  = JSON as! NSDictionary
                    let services:NSArray  = response["services"] as! NSArray
    
                    if services.count > 0{
                        for index in 0...(services.count-1) {
                            
                            let name = (services[index] as AnyObject).object(forKey: "name") as! NSString
                            
                            let id = (services[index] as AnyObject).object(forKey: "id") as! NSInteger
                            
                            do{
                                let realm = try! Realm()
                                
                                let newService = Service()
                                
                                newService.id = Int(id)
                                newService.name = name as String
                                
                                try realm.write({ () -> Void in
                                    self.services.append(name as String)
                                    print("Guardo Nuevo Servicio \(name) con id: \(id)")
                                    realm.add(newService)
                                })
                                
                            }catch{
                                
                                
                            }
                            
                        }
                    }else{
                        print("Servicio son 0")
                    }
                    
                    self.getCompanies()
               
                }
        }
        
        
    }
    
    func getCompanies(){
        
        Alamofire.request("http://www.waitless.cl/company/",method: .get, parameters: nil)
            .responseJSON { response in
                
                if let JSON = response.result.value {
                    //print("JSON: \(JSON)")
                    
                    let response                  = JSON as! NSDictionary
                    let companies:NSArray  = response["companies"] as! NSArray
                    
                    if companies.count > 0{
                        for index in 0...(companies.count-1) {
                            
                            let name = (companies[index] as AnyObject).object(forKey: "name") as! NSString
                            let service = (companies[index] as AnyObject).object(forKey: "service") as! NSString
                            let tag = (companies[index] as AnyObject).object(forKey: "tag") as! NSString
                            let id = (companies[index] as AnyObject).object(forKey: "id") as! NSInteger
                            let imgURL = (companies[index] as AnyObject).object(forKey: "imgURL") as! NSString
                            
                            // Get the default Realm
                            let realm = try! Realm()
                            let branches = realm.objects(Company.self).filter("id == %@",id)
                            
                            if branches.count > 0 {
                            }else{
                                do{
                                    let realm = try! Realm()
                                    
                                    let newCompany = Company()
                                    
                                    newCompany.id = Int(id)
                                    newCompany.name = name as String
                                    newCompany.service = service as String
                                    newCompany.tag = tag as String
                                    newCompany.imgURL = imgURL as String
                                    try realm.write({ () -> Void in
                                        print("Guardo Compañia \(name) con id: \(id)")
                                        realm.add(newCompany)
                                    })
                                    
                                }catch{
                                    
                                    
                                }
                            }
                            
                        }

                    }else{
                        print("Company son 0")
                    }
                    
                    
                    
                    self.checkCompaniesforBranches()
                    
                }
        }
        
        
    }
    
    func checkCompaniesforBranches(){
        
        // Get the default Realm
        let realm = try! Realm()
        
        
        let companies = realm.objects(Company.self)
        
        if companies.count > 0 {
            
            for index in 0...(companies.count-1) {
                self.getBranches(id: companies[index].id)
            }
        }
    }
    
    
    func getBranches(id: Int){
       
        Alamofire.request("http://www.waitless.cl/company/"+String(id)+"/branch",method: .get, parameters: nil)
            .responseJSON { response in
                
                if let JSON = response.result.value {
                    //print("JSON: \(JSON)")
                    
                    let response                  = JSON as! NSDictionary
                    let branches:NSArray  = response["branches"] as! NSArray
                    
                    
                    if branches.count > 0{
                        for index in 0...(branches.count-1) {
                            
                            let name = (branches[index] as AnyObject).object(forKey: "name") as! NSString
                            let company_id = (branches[index] as AnyObject).object(forKey: "company_id") as! NSString
                            let address = (branches[index] as AnyObject).object(forKey: "address") as! NSString
                            
                            let id = (branches[index] as AnyObject).object(forKey: "id") as! NSInteger
                            
                            
                            let lat = (branches[index] as AnyObject).object(forKey: "lat") as! NSString
                            
                            let lon = (branches[index] as AnyObject).object(forKey: "lon") as! NSString
                            
                            // Get the default Realm
                            let realm = try! Realm()
                            let branches = realm.objects(Branches.self).filter("id == %@",id)
                            
                            if branches.count > 0 {
                                
                            }else{
                                
                                do{
                                    let realm = try! Realm()
                                    
                                    let newBranch = Branches()
                                    
                                    newBranch.id = id
                                    newBranch.name = name as String
                                    newBranch.company_id = company_id as String
                                    newBranch.address = address as String
                                    newBranch.lat = lat as String
                                    newBranch.lon = lon as String
                                    
                                    try realm.write({ () -> Void in
                                        print("Guardo Nueva Branch \(name) con id: \(id)")
                                        realm.add(newBranch)
                                    })
                                    
                                }catch{
                                    
                                    
                                }
                            }
                            
                            
                        }
                        
                    }else{
                        print("branches son 0")
                    }
                   
                    self.checkQueuesforBranches()
                    
                    
                    
                }
                
                
        }
        
        
    }
    
    
    func checkQueuesforBranches(){
        
        // Get the default Realm
        let realm = try! Realm()
        
        
        let branches = realm.objects(Branches.self)
        
        if branches.count > 0 {
            
            for index in 0...(branches.count-1) {
                self.getQueues(idBranch: branches[index].id,idCompany: branches[index].company_id)
            }
        }
    }
    
    func getQueues(idBranch: Int, idCompany: String){
        //http://www.waitless.cl/company/5/branch/11/queue
       
        Alamofire.request("http://www.waitless.cl/company/"+idCompany+"/branch/"+String(idBranch)+"/queue",method: .get, parameters: nil)
            .responseJSON { response in
                
                if let JSON = response.result.value {
                    //print("JSON: \(JSON)")
                    
                    let response                  = JSON as! NSDictionary
                    let queue:NSArray  = response["queue"] as! NSArray
                    
                    if queue.count > 0{
                        for index in 0...(queue.count-1) {
                            
                            let name = (queue[index] as AnyObject).object(forKey: "name") as! NSString
                            
                            let id = (queue[index] as AnyObject).object(forKey: "id") as! NSInteger
                            
                            let id_branch = (queue[index] as AnyObject).object(forKey: "id_branch") as! NSString
                            
                            
                            // Get the default Realm
                            let realm = try! Realm()
                            let queues = realm.objects(Queues.self).filter("id == %@",id)
                            
                            if queues.count > 0 {
                                
                            }else{
                                
                                do{
                                    let realm = try! Realm()
                                    
                                    let newQueue = Queues()
                                    
                                    newQueue.id = id
                                    newQueue.name = name as String
                                    newQueue.id_branch = id_branch as String
                                    
                                    
                                    try realm.write({ () -> Void in
                                        print("Guardo Nueva Queue \(name) con id: \(id)")
                                        realm.add(newQueue)
                                    })
                                    
                                }catch{
                                    
                                    
                                }
                            }
                            
                            
                        }
                        
                        
                    }else{
                        print("Queues son 0")
                    }
                    
                    PKHUD.sharedHUD.hide(afterDelay: 1.0) { progress in
                        // Completion Handler
                      
                        self.menuTab()
                    }
                    
                    
                    
                    
                }
                
                
        }
        
        
    }

    
    func loadPointsMaps(id: String){
        
        self.names.removeAll(keepingCapacity: true)
        self.ides.removeAll(keepingCapacity: true)
        self.coordinatesLat.removeAll(keepingCapacity: true)
        self.coordinatesLong.removeAll(keepingCapacity: true)
        
        // Get the default Realm
        let realm = try! Realm()
        
        let idInt = Int(id)!
      
        let company = realm.objects(Company.self).filter("id == %@",idInt)
        imagesCompany = company.first?.imgURL
        
        let branches = realm.objects(Branches.self).filter("company_id == %@",id)
        
        
        if branches.count > 0 {
          
            
            for index in 0...(branches.count-1){
      
                names.append(branches[index].name)

                ides.append(branches[index].id)
                
                coordinatesLat.append(Double(branches[index].lat)!)
                
                coordinatesLong.append(Double(branches[index].lon)!)
            
            }
            
        }
  
        self.mapDetail.delegate = self
        
        for i in 0...(names.count-1)
        {
            
            let coordinate = CLLocationCoordinate2DMake(coordinatesLat[i], coordinatesLong[i])
            //let annotation = MKPointAnnotation()
            let annotation = CustomPointAnnotation()
            annotation.coordinate = coordinate
            annotation.title = names[i]
            annotation.imageName = imagesCompany
            //annotation.imageName = "profile"
            annotation.subtitle = "Watless: presiona para pedir tu turno"
            self.mapDetail.addAnnotation(annotation)
            
        }
        
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
      
        if segue.identifier == "showCompany" {
            let companyTableNav = segue.destination as! UINavigationController
            let companyTable = companyTableNav.topViewController as! CompanyTableViewController
            companyTable.idService = Int(idService)
            
       }else{
            let queueRequest = segue.destination as! UINavigationController
            let queueRe = queueRequest.topViewController as! QueuesTableViewController
            queueRe.branchID = brancheId
        }
    }
    
}

