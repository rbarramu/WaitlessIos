//
//  ProfileViewController.swift
//  Sirop
//
//  Created by Daniel Romero on 8/19/16.
//  Copyright © 2016 Daniel Romero. All rights reserved.
//

import UIKit
import CoreLocation
import Eureka
import RealmSwift
import BTNavigationDropdownMenu

class ProfileViewController: FormViewController {

    var menuView: BTNavigationDropdownMenu!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        menuTab()
        
        form +++  Section() {
            var header = HeaderFooterView<EurekaLogoViewNib>(HeaderFooterProvider.nibFile(name: "EurekaSectionHeader", bundle: nil))
            header.onSetupView = { (view, section) -> () in
                
                
                view.photoProfile.alpha = 0;
                UIView.animate(withDuration: 2.0, animations: { [weak view] in
                    view?.photoProfile.alpha = 1
                    })
                view.layer.transform = CATransform3DMakeScale(0.9, 0.9, 1)
                UIView.animate(withDuration: 1.0, animations: { [weak view] in
                    view?.layer.transform = CATransform3DIdentity
                    })
 
            }
            $0.header = header
            }
            
        +++ Section(header:"", footer: "Esta App fue desarrollada por Waitless")
            
            
        
            <<< ButtonRow() {
                $0.title = "Salir"
                }.onCellSelection({ (cell, row) in
                    
                    let refreshAlert = UIAlertController(title: "¿ Estas seguro ?", message: "", preferredStyle: UIAlertControllerStyle.alert)
                    
                    refreshAlert.addAction(UIAlertAction(title: "Si", style: .default, handler: { (action: UIAlertAction!) in
                        print("Handle Ok logic here")
                        
                        
                        let realm = try! Realm()
                        try! realm.write {
                            realm.deleteAll()
                        }
                        
                        
                        let preferences = UserDefaults.standard
                        
                        preferences.removeObject(forKey: "RutUsuario")
                        //preferences.removeObject(forKey: "StatusNumeroRenovacion")
                        //preferences.removeObject(forKey: "NumeroPedidoRenovacion")
                        
                        
                        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
                        let mainView = storyBoard.instantiateViewController(withIdentifier: "Main") as! LoginViewController
                        
                        self.present(mainView, animated:true, completion:nil)
                        
                        let appDelegate = UIApplication.shared.delegate as! AppDelegate
                        appDelegate.window?.rootViewController = mainView
                      
                        
                    }))
                    
                    refreshAlert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (action: UIAlertAction!) in
                        print("Handle Cancel Logic here")
                    }))
                    
                    self.present(refreshAlert, animated: true, completion: nil)
                    
                }).cellSetup() {cell, row in
                    //cell.tintColor = UIColor.Colors.Orange
                    
                }
        
        
        
            <<< ButtonRow() {
                $0.title = "Tutorial"
                }.onCellSelection({ (cell, row) in
                
                    let storyboard : UIStoryboard = UIStoryboard(name: "TutorialMenu", bundle: nil)
                    let vc : TutorialMenuViewController = storyboard.instantiateViewController(withIdentifier: "TutorialMenu") as! TutorialMenuViewController
                    
                    self.present(vc, animated: true, completion: nil)
                
                
                }).cellSetup() {cell, row in
                    //cell.tintColor = UIColor.Colors.Orange
                    
                }

    
            
            <<< ButtonRow() {
                $0.title = "Web Waitless"
                }.onCellSelection({ (cell, row) in
                    
                    UIApplication.shared.openURL(URL(string: "http://www.waitless.cl")!)
                    
                }).cellSetup() {cell, row in
                    //cell.tintColor = UIColor.Colors.Orange
               
 
        }
         
    }
    
    func menuTab(){
        let items = [""]
        
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.navigationBar.barTintColor = UIColor(red: 119/255.0, green:186/255.0, blue:162/255.0, alpha: 1.0)
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.white]
        
        menuView = BTNavigationDropdownMenu(navigationController: self.navigationController, containerView: self.navigationController!.view, title: "Waitless", items: items as [AnyObject])
        menuView.cellHeight = 50
        menuView.cellBackgroundColor = self.navigationController?.navigationBar.barTintColor
        menuView.cellSelectionColor = UIColor(red: 0.0/255.0, green:160.0/255.0, blue:195.0/255.0, alpha: 1.0)
        menuView.shouldKeepSelectedCellColor = true
        menuView.cellTextLabelColor = UIColor.white
        menuView.cellTextLabelFont = UIFont(name: "Avenir-Heavy", size: 17)
        menuView.cellTextLabelAlignment = .left // .Center // .Right // .Left
        menuView.arrowPadding = 15
        menuView.animationDuration = 0.5
        menuView.maskBackgroundColor = UIColor.black
        menuView.maskBackgroundOpacity = 0.3
        menuView.isUserInteractionEnabled = false
        menuView.arrowImage =  UIImage(named: "iconReloj")
        //menuView.isHidden = true
        menuView.didSelectItemAtIndexHandler = {(indexPath: Int) -> () in
            print("Did select item at index: \(indexPath)")
           
        }
        
        self.navigationItem.titleView = menuView
    }

}


class EurekaLogoViewNib: UIView {
    
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var email: UILabel!
    @IBOutlet weak var photoProfile: UIImageView!
   
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        // Get the default Realm
      
        
    }
    
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
       
    }
}





