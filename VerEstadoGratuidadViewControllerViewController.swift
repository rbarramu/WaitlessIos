//
//  VerEstadoGratuidadViewControllerViewController.swift
//  Waitless
//
//  Created by Daniel Romero on 11-07-16.
//  Copyright © 2016 Debak. All rights reserved.
//

import UIKit
import Alamofire

class VerEstadoGratuidadViewControllerViewController: UIViewController {

    @IBOutlet weak var Titulo: UILabel!
    @IBOutlet weak var Pretitulo: UILabel!
    @IBOutlet weak var Numero: UILabel!
    @IBOutlet weak var Seccion: UILabel!
    @IBOutlet weak var MiNumero: UILabel!
    @IBOutlet weak var Actualiza: UIButton!
    @IBOutlet weak var TomarNumero: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tabBarController?.tabBar.hidden = false
        actualizar()
        
        let preferences = NSUserDefaults.standardUserDefaults()
        
        let currentLevelKey = "StatusNumeroGratuidad"
        
        if preferences.objectForKey(currentLevelKey) == nil {
            //  Doesn't exist
            
                Esconder()
            
        } else {
            let currentLevel = preferences.integerForKey(currentLevelKey)
            
            if currentLevel == 1{
                
                
            }else{
                
            }
            
        }
        
        setupNavigationBar()
        // Do any additional setup after loading the view.
    }
    
    func setupNavigationBar(){
        self.navigationItem.leftBarButtonItem = UIBarButtonItem()
        
        self.navigationItem.leftBarButtonItem!.tintColor = UIColor.whiteColor()
        self.navigationItem.leftBarButtonItem =  UIBarButtonItem(title: "Volver", style:   UIBarButtonItemStyle.Plain, target: self, action: #selector(VerEstadoGratuidadViewControllerViewController.btn_clickedLeft(_:)))
    }
    
    func btn_clickedLeft(sender: UIBarButtonItem) {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    func Esconder(){
        Seccion.hidden = true
        MiNumero.hidden = true
        //TituloTime.hidden = true
        //Tiempo.hidden = true
        TomarNumero.hidden = false
        
    }
    
    func Mostar(){
        Seccion.hidden = false
        MiNumero.hidden = false
        //TituloTime.hidden = false
        //Tiempo.hidden = false
        TomarNumero.hidden = true
        
    }

    
    override func viewWillAppear(animated: Bool) {
        
        self.tabBarController?.tabBar.hidden = false
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func actualizarVista(sender: AnyObject) {
        
        actualizar()
    }
    
    func actualizar(){
        
        Alamofire.request(.GET, "http://api.waitless.zeek.cl/queues/2", parameters: nil)
            .responseJSON { response in
                //print(response.request)  // original URL request
                //print(response.response) // URL response
                //print(response.data)     // server data
                //print(response.result)   // result of response serialization
                
                if let JSON = response.result.value {
                    print("JSON: \(JSON)")
                    
                    
                    let response                  = JSON as! NSDictionary
                    
                    let data:NSDictionary         = response.objectForKey("data") as! NSDictionary
                 
                    let current_number:NSString         = (data.objectForKey("current_number") as! NSString)
                    
                    let last_number:NSString         = (data.objectForKey("last_number") as! NSString)
                  
                    self.Numero.text = current_number as String
                    self.MiNumero.text = last_number as String
                }
        }
        
        
        
    }

    
}
