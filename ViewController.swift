//
//  ViewController.swift
//  Waitless
//
//  Created by Daniel Romero on 09-07-16.
//  Copyright © 2016 Debak. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var ingresaRol: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector("keyboardWillShow:"), name:UIKeyboardWillShowNotification, object: self.view.window)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector("keyboardWillHide:"), name:UIKeyboardWillHideNotification, object: self.view.window)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func Entrar(sender: AnyObject) {
        
        if ingresaRol.text == "201173074-7"{
       
            self.performSegueWithIdentifier("ok", sender: self)
        }else{
            
            self.performSegueWithIdentifier("error", sender: self)
        }
        
    }
    @IBAction func RenovacionBeneficioAction(sender: AnyObject) {
        
        
        let preferences = NSUserDefaults.standardUserDefaults()
        
        let currentLevelKey = "StatusNumeroRenovacion"
        
        if preferences.objectForKey(currentLevelKey) == nil {
            //  Doesn't exist
            self.performSegueWithIdentifier("Renovacion", sender: self)
            
        } else {
            let currentLevel = preferences.integerForKey(currentLevelKey)
            
            if currentLevel == 1{
                self.performSegueWithIdentifier("VerEstadoRenovacion", sender: self)
                
            }else{
                self.performSegueWithIdentifier("Renovacion", sender: self)
            }
            
        }
        
    }
    
    override func viewWillDisappear(animated: Bool) {
        NSNotificationCenter.defaultCenter().removeObserver(self, name: UIKeyboardWillShowNotification, object: self.view.window)
        NSNotificationCenter.defaultCenter().removeObserver(self, name: UIKeyboardWillHideNotification, object: self.view.window)
    }
    
    func keyboardWillHide(sender: NSNotification) {
        let userInfo: [NSObject : AnyObject] = sender.userInfo!
        let keyboardSize: CGSize = userInfo[UIKeyboardFrameBeginUserInfoKey]!.CGRectValue.size
        self.view.frame.origin.y += keyboardSize.height
    }
    
    func keyboardWillShow(sender: NSNotification) {
        let userInfo: [NSObject : AnyObject] = sender.userInfo!
        let keyboardSize: CGSize = userInfo[UIKeyboardFrameBeginUserInfoKey]!.CGRectValue.size
        let offset: CGSize = userInfo[UIKeyboardFrameEndUserInfoKey]!.CGRectValue.size
        
        if keyboardSize.height == offset.height {
            UIView.animateWithDuration(0.1, animations: { () -> Void in
                self.view.frame.origin.y -= keyboardSize.height
            })
        } else {
            UIView.animateWithDuration(0.1, animations: { () -> Void in
                self.view.frame.origin.y += keyboardSize.height - offset.height
            })
        }
    }
}

